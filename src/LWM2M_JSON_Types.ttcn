///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//  File:               LWM2M_JSON_Types.ttcn
//  Description:
//  Rev:                R1C
//  Prodnr:             CNL 113 859
//  Updated:            2023-03-13
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////
module LWM2M_JSON_Types
{
  type record LwM2M_JSON_Resources
  {
    charstring bn optional,
    LwM2M_JSON_Resource_List e
  }
  with { 
    encode "JSON" 
    optional "implicit omit"
  }
	
  type record of LwM2M_JSON_Resource LwM2M_JSON_Resource_List	
  with { encode "JSON" }
	
  type record LwM2M_JSON_Resource
  {
    charstring n optional,
    JSON_Numeric v optional,
    boolean bv optional,
    charstring sv optional,
    charstring ov optional
  }
  with {
    encode "JSON"
    optional "implicit omit"
  }
  
  type union JSON_Numeric
  {
    integer i,
    float f
  }
  with {
    encode "JSON"
    variant "JSON : as value"
  }
  
  type record of SenML_JSON_Resource SenML_JSON_Resources
  with { encode "JSON" }  
  
  type record SenML_JSON_Resource
  {
    charstring bn optional,
    charstring n,
    JSON_Numeric v optional,
    boolean vb optional,
    charstring vs optional,
    charstring vd optional
  }
  with {
    encode "JSON" 
    optional "implicit omit"
  }
  
  type enumerated JSON_BaseName_Type
  {
    FULL_PATH,
    OBJECT_OBJECTINSTANCE,
    JUST_OBJECT,
    EMPTY
  }
}
