///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//  File:               EPTF_LwM2M_Transport_Definitions.ttcn
//  Description:
//  Rev:                R1C
//  Prodnr:             CNL 113 859
//  Updated:            2023-03-13
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_LwM2M_Transport_Definitions
// 
//  Purpose:
//    This module contains the generic transport definitions of the LwM2M load generator
//
//  Component Diagram:
//    (see EPTF_LwM2M_Transport_Definitions.components.jpg)
//
//  See also:
//    - <EPTF_LwM2M_LGen_Definitions>
//    - <EPTF_LwM2M_Transport_User_CT>
//    - <EPTF_LwM2M_Transport_Provider_CT>
///////////////////////////////////////////////////////////////
/*****************************************************************
 @startuml EPTF_LwM2M_Transport_Definitions.components.jpg
   package "EPTF_LwM2M_Transport_Definitions" {
     class EPTF_LwM2M_Transport_User_CT {
       fcb_EPTF_LwM2M_Transport_sendMessage vf_EPTF_LwM2M_Transport_send
     }
     class EPTF_LwM2M_Transport_Provider_CT {
       fcb_EPTF_LwM2M_Transport_receiveMessage vf_EPTF_LwM2M_Transport_receiveMessage
       fcb_EPTF_LwM2M_Transport_receiveEvent fcb_EPTF_LwM2M_Transport_receiveEvent
     }
     EPTF_LwM2M_Transport_Provider_CT <-> EPTF_LwM2M_Transport_User_CT
  }
 @enduml
******************************************************************/
module EPTF_LwM2M_Transport_Definitions
{
  import from LightweightM2M_Types all;

  ///////////////////////////////////////////////////////////
  //  Type: fcb_EPTF_LwM2M_Transport_receiveMessage
  //
  //  Purpose:
  //    Call-back function type for reporting received <EPTF_LwM2M_PDU> LwM2M messages
  //
  //  Elements:
  //    pl_message  - *in* <EPTF_LwM2M_PDU> - received COAP message
  ///////////////////////////////////////////////////////////
  type function fcb_EPTF_LwM2M_Transport_receiveMessage(in EPTF_LwM2M_PDU pl_message) runs on self;

  ///////////////////////////////////////////////////////////
  //  Type: fcb_EPTF_LwM2M_Transport_receiveEvent
  //
  //  Purpose:
  //    Call-back function type for reporting received <EPTF_LwM2M_Event> LWM2M event
  //
  //  Elements:
  //    p_event  - *in* <EPTF_LwM2M_Event> - received LWM2M event
  ///////////////////////////////////////////////////////////
  type function fcb_EPTF_LwM2M_Transport_receiveEvent(in EPTF_LwM2M_Event pl_event) runs on self;

  ///////////////////////////////////////////////////////////
  //  Type: fcb_EPTF_LwM2M_Transport_sendMessage
  //
  //  Purpose:
  //    Call-back function type for sending a <EPTF_LwM2M_PDU> LwM2M message
  //
  //  Elements:
  //    pl_msg  - *in* <EPTF_LwM2M_PDU> - LwM2M message to be sent
  ///////////////////////////////////////////////////////////
  type function fcb_EPTF_LwM2M_Transport_sendMessage(in EPTF_LwM2M_PDU pl_msg) runs on self;

  ///////////////////////////////////////////////////////////
  //  Type: EPTF_LwM2M_PDU
  //
  //  Purpose:
  //    Encapsulates a <LWM2M_PDU> with the corresponding entity (and FSM) indices
  //
  //  Elements:
  //    pdu - <LWM2M_PDU> - LwM2M PDU
  //    eIdx - *integer* - entity index
  //    fsmIdx - *integer* - fsm index
  ///////////////////////////////////////////////////////////
  type record EPTF_LwM2M_PDU
  {
    LWM2M_PDU	pdu,
    integer     eIdx,
    integer	fsmIdx
  }

  ///////////////////////////////////////////////////////////
  //  Type: EPTF_LwM2M_Event
  //
  //  Purpose:
  //    Encapsulates a <EPTF_LwM2M_Event> with the corresponding entity
  //
  //  Elements:
  //    event - <LWM2M_Event> - LwM2M event
  //    eIdx - *integer* - entity index
  ///////////////////////////////////////////////////////////
  type record EPTF_LwM2M_Event
  {
    LWM2M_Event event,
    integer     eIdx
  }

  ///////////////////////////////////////////////////////////
  //  Type: LWM2M_Event
  //
  //  Purpose:
  //    A union of possible LwM2M events
  //
  //  Elements:
  //    resourceNotObserved - <ObjectPath> - dispatched when a resource is not observed any more, the object path identifies the resource
  //    atomicBlock1Finished - <LWM2M_Event_Block1> - in case an atomic block1 transfer finished, it is indicated here
  ///////////////////////////////////////////////////////////
  type union LWM2M_Event
  {
    ObjectPath          resourceNotObserved,
    LWM2M_Event_Block1  atomicBlock1Finished
  }

  ///////////////////////////////////////////////////////////
  //  Type: LWM2M_Event_Block1
  //
  //  Purpose:
  //    Blcok1 event descriptor
  //
  //  Elements:
  //    path - <ObjectPath> - path uri
  //    method - <LWM2M_Block1_Method> - method of the request
  //    contentFormat - integer - content format code
  //    resources - <LwM2M_Resource_List> - resource list
  ///////////////////////////////////////////////////////////
  type record LWM2M_Event_Block1
  {
    ObjectPath	        path,
    LWM2M_Block1_Method method,
    integer             contentFormat,
    LwM2M_Resource_List resources
  }

  ///////////////////////////////////////////////////////////
  //  Enum: LWM2M_Block1_Method
  //
  //  Purpose:
  //    Block1 method types
  //
  //  Elements:
  //    - UNMAPPED
  //    - WRITE
  ///////////////////////////////////////////////////////////
  type enumerated LWM2M_Block1_Method
  {
    UNMAPPED,
    WRITE
  }

  const LWM2M_Event_Block1 c_LWM2M_Event_Block1_empty :=
  {
    path := {},
    method := UNMAPPED,
    contentFormat := -1,
    resources := {}
  }

  /*****************************************************************
   @startuml EPTF_LwM2M_Transport_Definitions.EPTF_LwM2M_Transport_Provider_CT.jpg
     class EPTF_LwM2M_Transport_Provider_CT {
       fcb_EPTF_LwM2M_Transport_receiveMessage vf_EPTF_LwM2M_Transport_receiveMessage
       fcb_EPTF_LwM2M_Transport_receiveEvent fcb_EPTF_LwM2M_Transport_receiveEvent
     }
   @enduml
  *****************************************************************/
  ///////////////////////////////////////////////////////////
  // Type: EPTF_LwM2M_Transport_Provider_CT
  //
  // Purpose:
  //   Base component type for a LwM2M transport realization
  //
  // Class:
  //   (see EPTF_LwM2M_Transport_Definitions.EPTF_LwM2M_Transport_Provider_CT.jpg)
  //
  // Variables:
  //   vf_EPTF_LwM2M_Transport_receiveMessage - <fcb_EPTF_LwM2M_Transport_receiveMessage> - Function hook for reporting received messages
  //   vf_EPTF_LwM2M_Transport_receiveEvent - <fcb_EPTF_LwM2M_Transport_receiveEvent> - Function hook for reporting received events
  ///////////////////////////////////////////////////////////
  type component EPTF_LwM2M_Transport_Provider_CT
  {
    var fcb_EPTF_LwM2M_Transport_receiveMessage  vf_EPTF_LwM2M_Transport_receiveMessage := null;
    var fcb_EPTF_LwM2M_Transport_receiveEvent  vf_EPTF_LwM2M_Transport_receiveEvent := null;
  }

  /*****************************************************************
   @startuml EPTF_LwM2M_Transport_Definitions.EPTF_LwM2M_Transport_User_CT.jpg
     class EPTF_LwM2M_Transport_User_CT {
       fcb_EPTF_LwM2M_Transport_sendMessage vf_EPTF_LwM2M_Transport_send
     }
   @enduml
  *****************************************************************/
  ///////////////////////////////////////////////////////////
  // Type: EPTF_LwM2M_Transport_User_CT
  //
  // Purpose:
  //   Base component type for a LwM2M transport user realization
  //
  // Class:
  //   (see EPTF_LwM2M_Transport_Definitions.EPTF_LwM2M_Transport_User_CT.jpg)
  //
  // Variables:
  //   vf_EPTF_LwM2M_Transport_send - <fcb_EPTF_LwM2M_Transport_sendMessage> - Function hook for reporting received messages
  ///////////////////////////////////////////////////////////
  type component EPTF_LwM2M_Transport_User_CT
  {
    var fcb_EPTF_LwM2M_Transport_sendMessage     vf_EPTF_LwM2M_Transport_send := null;
  }
}
