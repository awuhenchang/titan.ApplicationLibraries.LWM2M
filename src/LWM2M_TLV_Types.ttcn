///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//  File:               LWM2M_TLV_Types.ttcn
//  Description:
//  Rev:                R1C
//  Prodnr:             CNL 113 859
//  Updated:            2023-03-13
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////
module LWM2M_TLV_Types
{
  type record LWM2M_TLV_Resource
  {
    TLV_Type  tlv_type,
    integer   id,
    integer   length_of_value,
    TLV_Value tlv_value
  }
  
  type record of LWM2M_TLV_Resource LWM2M_TLV_Resources;

  type record TLV_Type
  {
    integer     length_of_value,
    integer     type_of_length,
    integer     length_of_id,
    TLV_Id_Type type_of_id
  }
  
  type enumerated TLV_Id_Type
  {
    OBJECT_INSTANCE,
    RESOURCE_INSTANCE,
    MULTIPLE_RESOURCE,
    VALUE
  }
  
  type union TLV_Value
  {
    LWM2M_TLV_Resources internal_tlvs,
    bitstring raw_resource_value
  }

  type enumerated Value_Type
  {
    INTEGER,
    FLOAT,
    STRING,
    OPAQUE,
    BOOL
  }

  type integer TLV_integer with { encode "RAW"; variant "COMP(2scompl)" };

  type enumerated isBootstrap
  {
    NO_BOOTSTRAP,
    BOOTSTRAP
  }
	
  type record ResourceObject {
    integer objId,
    ResourceType_List resType
  }
	
  type record ResourceType {
    integer resId,
    Value_Type valType
  }
	
  type record of ResourceObject ResourceObj_List;
  type record of ResourceType ResourceType_List;
	
  const ResourceObj_List c_LWM2M_Resource_Types := {
    {0, c_LWM2M_Security_Resource_Types},
    {1, c_LWM2M_Server_Resource_Types},
    {5, c_LWM2M_FirmwareUpdate_Resource_Types }
  }
	
  const ResourceType_List c_LWM2M_Security_Resource_Types := {
    {0, STRING},
    {1, BOOL},
    {2, INTEGER},
    {3, OPAQUE},
    {4, OPAQUE},
    {5, OPAQUE},
    {6, INTEGER},
    {7, OPAQUE},
    {8, OPAQUE},
    {9, STRING},
    {10, INTEGER},
    {11, INTEGER},
    {12, INTEGER}
  }
  
  const ResourceType_List c_LWM2M_Server_Resource_Types := {
    {0, INTEGER},
    {1, INTEGER},
    {2, INTEGER},
    {3, INTEGER},
    {5, INTEGER},
    {6, BOOL},
    {7, STRING}
  }
  
  const ResourceType_List c_LWM2M_FirmwareUpdate_Resource_Types := {
    {0, OPAQUE},
    {1, STRING},
    {2, OPAQUE},
    {3, INTEGER},
    {5, INTEGER},
    {6, STRING}
  }
}
