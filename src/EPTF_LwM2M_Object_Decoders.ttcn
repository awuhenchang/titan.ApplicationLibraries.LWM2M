///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//  File:               EPTF_LwM2M_Object_Decoders.ttcn
//  Description:
//  Rev:                R1C
//  Prodnr:             CNL 113 859
//  Updated:            2023-03-13
//  Contact:            http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_LwM2M_Object_Definitions
// 
//  Purpose:
//    This module contains the smart object decoder functions
//
///////////////////////////////////////////////////////////////
module EPTF_LwM2M_Object_Decoders
{
  import from EPTF_LwM2M_Object_Definitions all;
  import from LightweightM2M_Types all;
  import from TCCConversion_Functions all;  
  
  ///////////////////////////////////////////////////////////
  //  Function: f_LwM2M_Resource_decode
  //
  //  Purpose:
  //    Decodes a resource based on a contnet format and its spceification
  //
  //  Parameters:
  //    p_res - *inout* <LwM2M_Resource> - the resource to be decoded (it will be updated)
  //    p_spec - *in* <LwM2M_ResourceSpecification> - the specification of the resource
  //    p_contentFormat - *in integer* - the content format code
  //
  //  Related Type:
  //    <LwM2M_Resource>
  ///////////////////////////////////////////////////////////
  function f_LwM2M_Resource_decode(
    inout LwM2M_Resource p_res,
    in LwM2M_ResourceSpecification p_spec,
    in integer p_contentFormat
  )
  return boolean
  {
    @try
    {
      // Plain text
      if (p_contentFormat == 0 and ischosen(p_res.val.strValue))
      {
        var charstring v_incoming := p_res.val.strValue;
        if (p_spec.type_ == STRING)
        {
          // No change needed
          return true;
        }
        else if (p_spec.type_ == FLOAT)
        {
          p_res.val.floatValue := str2float(v_incoming);
          return true;
        }
        else if (p_spec.type_ == INTEGER)
        {
          p_res.val.intValue := str2int(v_incoming);
          return true;
        }
        else if (p_spec.type_ == OPAQUE)
        {
          p_res.val.opaqueValue := str2oct(v_incoming);
          return true;
        }
        return false;
      }
    
      // Opaque
      else if (p_contentFormat == 42 and ischosen(p_res.val.opaqueValue))
      {
        if (p_spec.type_ == OPAQUE)
        {
          // No change needed
          return true;
        }
      }
    }
    @catch(err) {}
    
    return false;
  }
}
